const fs=require('fs');
const dataMatches=require('/home/prateek/work/javascript/Project_IPL/src/data/resultMatches.json');

let res={};

dataMatches.filter((element)=>{
    let year=element.season;
    let name=element.player_of_match;
    if(res[year]==undefined){
        let obj={};
        obj[name]=1;
        res[year]=obj;
    }
    else{
        if(res[year][name]==undefined){
            res[year][name]=1;
        }
        else{
            res[year][name]++;

        }
    }

    if(res[year]['max_win']==undefined){
        res[year]['max_win']=res[year][name];
    }
    else{
        res[year]['max_win']=Math.max(res[year]['max_win'],res[year][name]);
    }
})


Object.keys(res).map((year)=>{
    let obj={};
    Object.keys(res[year]).filter((name)=>{
        if(res[year][name]==res[year]['max_win'] && name != 'max_win'){
            obj[name]=res[year][name];
        }
    })
    res[year]=obj;
})


res=JSON.stringify(res,null,2);
fs.writeFileSync('/home/prateek/work/javascript/Project_IPL/src/public/output/playerOfMatchAward.json',res);