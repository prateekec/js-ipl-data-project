const fs=require('fs');
const dataMatches=require('/home/prateek/work/javascript/Project_IPL/src/data/resultMatches.json');
const dataDeleveries=require('/home/prateek/work/javascript/Project_IPL/src/data/resultDeleveries.json');


let ids=new Set();
dataMatches.map((element)=>{
    if(element.season==2016){
        ids.add(element.id);
    }
})


let res={};

dataDeleveries.filter((element)=>{
    if(ids.has(element.match_id)){
        let team=element.bowling_team;
        let runs=parseInt(element.extra_runs);
        if(res[team]==undefined){
            res[team]=runs;
        }
        else{
            res[team]+=runs;
        }
    }
})


res=JSON.stringify(res,null,2);
fs.writeFileSync('/home/prateek/work/javascript/Project_IPL/src/public/output/extraRunPerTeamIn2016.json',res);
