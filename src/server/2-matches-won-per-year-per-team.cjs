const fs=require('fs');
const dataMatches=require('/home/prateek/work/javascript/Project_IPL/src/data/resultMatches.json');


let res={};

dataMatches.filter((element)=>{
    let year=element.season;
    let team=element.winner;
    if(res[year]==undefined){
        let obj={};
        obj[team]=1;
        res[year]=obj;
    }
    else{
        if(res[year][team]==undefined){
            res[year][team]=1;
        }
        else{
            res[year][team]++;
        }
    }
})


res=JSON.stringify(res,null,2);
fs.writeFileSync('/home/prateek/work/javascript/Project_IPL/src/public/output/matchesWonPerYearPerTeam.json',res);