const fs = require('fs');
const Papa = require('papaparse');

function csvToJson(csvFilePath) {
  const csvData = fs.readFileSync(csvFilePath, 'utf8');
  const jsonData = Papa.parse(csvData, {
    header: true, // Treat the first row as headers
    skipEmptyLines: true, // Skip empty lines
  });

  return jsonData.data;
}


let resultDeleveries=csvToJson('/home/prateek/work/javascript/Project_IPL/src/data/deliveries.csv');
let resultMatches=csvToJson('/home/prateek/work/javascript/Project_IPL/src/data/matches.csv');
resultDeleveries=JSON.stringify(resultDeleveries,null,2);
resultMatches=JSON.stringify(resultMatches,null,2);
fs.writeFileSync('/home/prateek/work/javascript/Project_IPL/src/data/resultDeleveries.json',resultDeleveries);
fs.writeFileSync('/home/prateek/work/javascript/Project_IPL/src/data/resultMatches.json',resultMatches);
// console.log(resultDeleveries);


// const fs = require('fs');
// function parse(path) {
//     let csv=fs.readFileSync(path);
//     let arr=csv.toString();
//     arr=arr.split('\n')
//     // console.log(typeof(arr));

//     const header=arr[0].split(',');

//     let resultDeleveries=[];
//     // let count =10;
//     for(let row in arr){
//         // if(count<=0) break;
//         // count-=1;
//         if(row==0) continue
//         let elements=arr[row].split(',');
//         // console.log(elements);
//         let obj={};
//         for(let idx in elements){
//             obj[header[idx]]=elements[idx];
//         }
//         resultDeleveries.push(obj);

//     }
//     return resultDeleveries;
// }

// let resultDeleveries=parse('/home/prateek/work/javascript/Project_IPL/src/data/deliveries.csv');
// let resultMatches=parse('/home/prateek/work/javascript/Project_IPL/src/data/matches.csv');
// // console.log(resultMatches);

// module.exports={resultDeleveries,resultMatches};